import { LitElement, html} from "lit-element"
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'
import '../persona-main-dm/persona-main-dm.js'
class PersonaMain extends LitElement{

    static get properties(){
        return{
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }
    constructor(){
        super();
       this.people = [];
       /* this.people = [
            {
                name: "Kutxi Romero",
                yearsInCompany:10,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Kutchi Romero"
                },
                profile: "lorem ipsum1"
            },
            {
                name: "Hakeem Olajuwon",
                yearsInCompany: 5,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Hakeem"
                },
                profile: "lorem ipsum2"
            },
            {
                name: "Hellboy",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Hellboy"
                },
                profile: "lorem ipsum3"
            },
            {
                name: "Chiquito de la calzada",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Hellboy"
                },
                profile: "lorem ipsum4"
            },
            {
                name: "Almudenas Grandes",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Hellboy"
                },
                profile: "lorem ipsum5"
            }
        ]*/
           this.showPersonForm = false;
    }
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <h2 class="text-center">Personas</h2>
        
        <div class="row" id="peopleList">
            <div class="row row-cols-1 row-cols-sm-4">
        ${this.people.map(
            person => html` <persona-ficha-listado 
            fname="${person.name}" 
            yearsInCompany="${person.yearsInCompany}" 
            .photo="${person.photo}" 
            profile="${person.profile}"
            @delete-person="${this.deletePerson}"
            @info-person="${this.infoPerson}">
            </persona-ficha-listado>`
        )}
           </div>   
        </div>
        <div class="row">
            <persona-form 
            @persona-form-close="${this.personFormClose}"   
            @persona-form-store="${this.personFormStore}" 
            class="d-none border rounder border-primary" id="personForm">
            </persona-form>
        </div>
        <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"> 
        </persona-main-dm>
        `;
    }
    updated(changedProperties){
        console.log("updated");
        if(changedProperties.has("showPersonForm")){
            console.log("ha cambiado el valor de la propiedad showPersonForm en persona-main")
            if(this.showPersonForm ===true){
                this.showPersonFormData();
            }else{
                this.showPersonList();
            }

        }
        if(changedProperties.has("people")){
            console.log("he cambiado el valor de la propiedad people")
            this.dispatchEvent(
                new CustomEvent(
                    "updated-people",{
                        detail : {
                            people : this.people
                        }
                    }
                )
            )
        }
    }
    
    peopleDataUpdated(e){
        console.log("peopleUpdated");
        this.people = e.detail.people;
    }
    infoPerson(e){
        console.log("infoPerson");
        console.log("se ha pedido más info " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        ); 
            console.log("changedPerson");
            //console.log("changedPerson" + chosenPerson[0].name);
            let person = {};
            person.name = chosenPerson[0].name;
            person.profile = chosenPerson[0].profile;
            person.yearsInCompany = chosenPerson[0].yearsInCompany;
            console.log("changedPerson" + chosenPerson[0].yearsInCompany);
            this.shadowRoot.getElementById("personForm").person = person;
            this.shadowRoot.getElementById("personForm").editingPerson = true;
            this.showPersonForm = true;
    }
    deletePerson(e){
        console.log("deletePerson en persona");
        console.log(" se va a borrar la persona de nombre " + e.detail.name);
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }
    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando formulario");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }
    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }
    personFormClose(){
        console.log("personaFormClose");
        console.log("Cerrando form");
  
    }
    personFormStore(e){
        console.log("personaFormStore");
        
        console.log(e.detail.person);
        if(e.detail.editingPerson === true){
           console.log("actualizando nombre " +e.detail.person.name)
           this.people = this.people.map(
                person => person.name === e.detail.person.name
                ? person = e.detail.person: person );

           /*
            let indexOfPerson =this.people.findIndex(
                person => person.name === e.detail.person.name
            );
            if (indexOfPerson >= 0){
                console.log("encontrado");
                this.people[indexOfPerson] = e.detail.person;
            }*/
        }else{
            console.log("almacenar nuevo");
            this.people = [...this.people, e.detail.person];
            //ADD YA ESTABA mismo array con el nuevo al final
           // this.people.push(e.detail.person);
            
        }
       // this.people.push(e.detail.person);
        console.log("persona almacenada");
        this.showPersonForm = false;
    }
}

customElements.define('persona-main', PersonaMain);