import { LitElement, html} from "lit-element";


class FichaPersona extends LitElement{
    static get properties(){
        return {
            name: {type: String},
            yearsInCompany: {type:Number},
            personInfo: {type: String}
        }
    }
    constructor(){
        super();
        this.name="prueba";
        this.yearsInCompany=17;
        this.updatePersonInfo();
    }
    updated(changedProperties){
        console.log("updated");
        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + "cambia el valor " + oldValue);
        })
        if(changedProperties.has("name")){
            console.log("Propiedad name cambia valor " + changedProperties.get("name") + "cambia el valor " + this.name);
        }
        if(changedProperties.has("yearsInCompany")){
            console.log("Propiedad " + changedProperties.get("personInfo") + " cambia " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }
    render(){
        return html`
        <div>
            <label>Nombre Completo</label>
            <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
            <br />
            <label>Años en la empresa</label>
            <input type="text" value="${this.yearsInCompany}" @input="${this.updateYears}"></input>
            <br />
            <input type="text" value="${this.personInfo}" disabled></input>
            <br />      
                 
        </div>
        `;
    }
    updateName(e){
        console.log("aaa");
        this.name = e.target.value;
    }
    updateYears(es){
        console.log("bb");
        this.yearsInCompany = es.target.value;
        
    }
    updatePersonInfo(){
        if(this.yearsInCompany >=7){
            this.personInfo= "lead";
            } else if(this.yearsInCompany >=5){
                this.personInfo= "senior";   
            }else if(this.yearsInCompany >=3){
                this.personInfo= "team";
            }else{
                this.personInfo= "junior";
            }
    }
}

customElements.define('ficha-persona', FichaPersona);