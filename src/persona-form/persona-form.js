import { LitElement, html} from "lit-element";


class PersonaForm extends LitElement{
    static get properties(){
        return{
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }
    constructor(){
        super();
        this.resetFormData();
    }
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div>
            <form>
            <div class="form-group">
               <label>Nombre completo</label>
               <input @input="${this.updateName}" type="text" class="form-control" placeHolder="nombre completo"
               .value="${this.person.name}"
               ?disabled="${this.editingPerson}""
               />
            </div>
            <div class="form-group">
                <label>Perfil</label>
                <textarea @input="${this.updateProfile}" class="form-control" placeHolder="perfil" rows="5"
                .value="${this.person.profile}"></textarea>
            </div>
            <div class="form-group">
                <label>Años en la empresa</label>
                <input @input="${this.updateYearsInCompany}" type="text" class="form-control" placeHolder="Años en la empresa"
                .value="${this.person.yearsInCompany}"/>
            </div>
                <button @click="${this.goBack}" class="btn btn-primary"><strong>atras</strong></button>
                <button @click="${this.storePerson}" class="btn btn-success"><strong>guardar</strong></button>
            </form>
        </div>
        `;
    }
    updateName(e){
        console.log("updateName");
        console.log("actualizando la propiedad name con el valor " +e.target.value);
        this.person.name = e.target.value;
    }
    updateProfile(e){
        console.log("updateProfile");
        console.log("actualizando la propiedad profile con el valor " +e.target.value);
        this.person.profile = e.target.value;
    }
    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        console.log("actualizando la propiedad yearsInCompany con el valor " +e.target.value);
        this.person.yearsInCompany = e.target.value;
    }
    goBack(e){
        console.log("goback");
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
        
    }
    resetFormData(){
        console.log("resetFormData");
        this.person ={};
        this.person.name ="";
        this.person.profile ="";
        this.person.yearsInCompany ="";
        this.editingPerson = false;
        }
    storePerson(e){
        console.log("storePerso");
        
        e.preventDefault();
        console.log("la propiedade name " + this.person.name + " la propiedad profile " + this.person.profile + " y años "  + this.person.yearsInCompany);
        this.person.photo={
            src: "./img/persona.jpg",
            alt: "persona"
        }
        this.dispatchEvent(new CustomEvent(
            "persona-form-store",
            {
              detail: { 
                  person: {
                      name: this.person.name,
                      profile: this.person.profile,
                      yearsInCompany: this.person.yearsInCompany,
                      photo: this.person.photo
                  }, 
                  editingPerson: this.editingPerson
              }
            }
        ))
        
    }
}

customElements.define('persona-form', PersonaForm);