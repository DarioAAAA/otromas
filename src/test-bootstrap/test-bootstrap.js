import { LitElement, html, css} from "lit-element";


class TestBootstrap extends LitElement{
    static get styles(){
        return css`
        .redbg{
            background-color: red;
        }
        .greybg{
            background-color: grey;
        }
        `

        ;
    }
    constructor(){
        super();
    }
    render(){
        return html`
        
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <h1>Test bootstrap</h1>
        <div class="row greybg"> 
            <div class="col redbg">col 1
            </div>
            <div class="col">col 2
            </div>
            <div class="col">col 3 
            </div>
        </div>
        `;
    }
}

customElements.define('test-bootstrap', TestBootstrap);