import { LitElement, html} from "lit-element";


class PersonaStats extends LitElement{
    static get properties(){
        return{
            people: {type: Array}
        };
    }
    constructor(){
        super();
        this.people = [];

    }
    updated(changedProperties){
        console.log("updated en persona stast")
        console.log(changedProperties);
        if (changedProperties.has("people")){
            console.log("he cambiao el valor de la propiedad people en persona stats");
            let peopleStats = this.gathPeopleArrayInfo(this.people);
            console.log(peopleStats)
            this.dispatchEvent(
                new CustomEvent(
                    "updated-people-stats",
                    {
                        detail : {
                            peopleStats : peopleStats
                        }
                    }
                )
            )
        }
    }
    gathPeopleArrayInfo(people){
        console.log("gathPeopleArrayInfo");
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        return peopleStats;
    }
}

customElements.define('persona-stats', PersonaStats);