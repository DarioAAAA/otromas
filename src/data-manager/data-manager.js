import { LitElement, html} from "lit-element";


class DataManager extends LitElement{
    static get properties(){
        return{
            peoples: {type: Array}
        };
    }
    constructor(){
        super();
        this.people = [
            {
                name: "Kutxi Romero",
                yearsInCompany:10,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Kutchi Romero"
                },
                profile: "lorem ipsum1"
            },
            {
                name: "Hakeem Olajuwon",
                yearsInCompany: 5,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Hakeem"
                },
                profile: "lorem ipsum2"
            },
            {
                name: "Hellboy",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Hellboy"
                },
                profile: "lorem ipsum3"
            },
            {
                name: "Chiquito de la calzada",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Hellboy"
                },
                profile: "lorem ipsum4"
            },
            {
                name: "Almudenas Grandes",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Hellboy"
                },
                profile: "lorem ipsum5"
            }
        ] 
    }
   

    render(){
        return html`
        <h1>data-manager</h1>
        `;
    }
}

customElements.define('data-manager', DataManager);